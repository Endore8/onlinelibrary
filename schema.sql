BEGIN;

DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id       INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT NOT NULL,
  password TEXT NOT NULL
);

DROP TABLE IF EXISTS authors;

CREATE TABLE authors (
  id   INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

DROP TABLE IF EXISTS books;

CREATE TABLE books (
  id    INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT NOT NULL
);

DROP TABLE IF EXISTS authors_books;

CREATE TABLE authors_books (
  author_id INTEGER REFERENCES authors (id),
  book_id INTEGER REFERENCES books (id),
  UNIQUE (author_id, book_id)
);

INSERT INTO users ('username', 'password') VALUES ('user', '1111');

INSERT INTO books ('title') VALUES ('Book1');
INSERT INTO books ('title') VALUES ('Book2');
INSERT INTO books ('title') VALUES ('Book3');
INSERT INTO books ('title') VALUES ('Book4');
INSERT INTO books ('title') VALUES ('Book5');
INSERT INTO books ('title') VALUES ('Book6');
INSERT INTO books ('title') VALUES ('Book7');
INSERT INTO books ('title') VALUES ('Book8');

INSERT INTO authors ('name') VALUES ('Author1');
INSERT INTO authors ('name') VALUES ('Author2');
INSERT INTO authors ('name') VALUES ('Author3');
INSERT INTO authors ('name') VALUES ('Author4');
INSERT INTO authors ('name') VALUES ('Author5');

INSERT INTO authors_books ('book_id', 'author_id') VALUES (1, 1);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (1, 3);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (1, 5);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (2, 1);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (2, 2);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (3, 5);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (5, 5);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (6, 4);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (6, 5);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (7, 2);
INSERT INTO authors_books ('book_id', 'author_id') VALUES (8, 3);

COMMIT;
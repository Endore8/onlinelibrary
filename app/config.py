import os
_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = True

SECRET_KEY = '1fsG0#4@6kgdfgdf&*0'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, '../content.db')
DATABASE = os.path.join(_basedir, '../content.db')

CSRF_ENABLED = True
CSRF_SESSION_KEY = "!2435#fsdW*35Kwe"

TEMPLATES_ROOT = os.path.join(_basedir, 'templates/')
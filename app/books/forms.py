from wtforms import Form, StringField, IntegerField
from wtforms.validators import Required, ValidationError
from sqlalchemy import and_

from models import Book


class AddBookForm(Form):
    def book_exist(self, field):
        if Book.query.filter_by(title=self.title.data).first():
            raise ValidationError('Book already exists')

    title = StringField('Book', [Required(), book_exist])


class EditBookForm(Form):
    def book_exist(self, field):
        if Book.query.filter(and_(Book.title == self.title.data, Book.id != self.book_id.data)).first():
            raise ValidationError('Book already exists')

    book_id = IntegerField('Id', [Required()])
    title = StringField('Book', [Required(), book_exist])
from app import db

association_table = db.Table('authors_books',
                             db.Column('book_id', db.Integer, db.ForeignKey('books.id')),
                             db.Column('author_id', db.Integer, db.ForeignKey('authors.id'))
)


class Book(db.Model):
    __tablename__ = 'books'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    authors = db.relationship("Author",
                              secondary=association_table,
                              backref=db.backref("books", lazy="dynamic"))

    def __init__(self, title=None, authors=None):
        self.title = title
        self.authors = authors

    def __repr__(self):
        return '<Book %r>' % self.title
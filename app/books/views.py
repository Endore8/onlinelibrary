import json
from flask import Blueprint, request, g, render_template

from app import db
from forms import AddBookForm, EditBookForm
from models import Book
from app.authors.models import Author

mod = Blueprint('books', __name__, url_prefix='/books')


@mod.route('/', methods=['GET'])
def books():
    g._csrf_used = True
    q = request.args.get('q')
    if q:
        books = Book.query.filter(Book.title.like('%%%' + q + '%%')).all()
    else:
        books = Book.query.all()
    return render_template('books.html', books=books, authors=Author.query.all(), q=q or '')


@mod.route('/add', methods=['POST'])
def add_book():
    form = AddBookForm(request.form)
    if form.validate():
        authors = []
        for author_id in request.form.getlist('authors[]'):
            authors.append(Author.query.filter_by(id=author_id))
        book = Book(title=form.title.data, authors=authors)
        db.session.add(book)
        db.session.commit()
        return json.dumps({'success': True})
    return json.dumps({'success': False, 'error': form.title.errors[0]})


@mod.route('/edit', methods=['POST'])
def edit_book():
    form = EditBookForm(request.form)
    if form.validate():
        book = Book.query.filter_by(id=form.book_id.data).first()
        book.title = form.title.data
        authors = []
        for author_id in request.form.getlist('authors[]'):
            author_id = int(author_id)
            authors.append(Author.query.filter_by(id=author_id).first())
        book.authors = authors
        db.session.commit()
        return json.dumps({'success': True})
    return json.dumps({'success': False, 'error': form.title.errors[0]})


@mod.route('/remove', methods=['POST'])
def remove_book():
    book_id = request.form.get('book_id')
    if book_id:
        book = Book.query.filter_by(id=book_id).first()
        db.session.delete(book)
        db.session.commit()
    return json.dumps({'success': bool(book_id)})
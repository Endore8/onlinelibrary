from wtforms import Form, StringField, PasswordField, ValidationError
from wtforms.validators import Required, EqualTo

from models import User


class LoginForm(Form):
    def validate_login(self, field):
        user = User.query.filter_by(username=self.username.data).first()
        if not user or user.password != self.password.data:
            raise ValidationError('Enter valid username and/or password')

    username = StringField('Username', [Required(), validate_login])
    password = PasswordField('Password', [Required()])


class RegisterForm(Form):
    def validate_registration(self, field):
        if User.query.filter_by(username=self.username.data).first():
            raise ValidationError('User already exists')

    username = StringField('Username', [Required(), validate_registration])
    password = PasswordField('Password', [Required(), EqualTo('password', message='Passwords must match')])
    confirm = PasswordField('Repeat Password', [Required()])
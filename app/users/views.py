from flask import Blueprint, request, g, redirect, url_for, render_template
from flask.ext.login import LoginManager, login_user, logout_user, login_required

from app import app, db
from forms import RegisterForm, LoginForm
from models import User

mod = Blueprint('users', __name__, url_prefix='/users')
login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(userid):
    return User.query.filter_by(id=userid).first()


@mod.route('/login', methods=['GET', 'POST'])
def login():
    g._csrf_used = True
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User.query.filter_by(username=form.username.data).first()
        login_user(user)
        return redirect(url_for('index'))
    return render_template('login.html', form=form)


@mod.route('/register', methods=['GET', 'POST'])
def register():
    g._csrf_used = True
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User()
        form.populate_obj(user)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    return render_template('register.html', form=form)


@mod.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('index'))
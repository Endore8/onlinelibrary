import json
from flask import Blueprint, request, g, render_template

from app import db
from forms import AddAuthorForm, EditAuthorForm
from models import Author

mod = Blueprint('authors', __name__, url_prefix='/authors')


@mod.route('/', methods=['GET'])
def authors():
    g._csrf_used = True
    q = request.args.get('q')
    if q:
        authors = Author.query.filter(Author.name.like('%%%' + q + '%%')).all()
    else:
        authors = Author.query.all()
    return render_template('authors.html', authors=authors, q=q or '')


@mod.route('/add', methods=['POST'])
def add_author():
    form = AddAuthorForm(request.form)
    if form.validate():
        author = Author()
        form.populate_obj(author)
        db.session.add(author)
        db.session.commit()
        return json.dumps({'success': True})
    return json.dumps({'success': False, 'error': form.name.errors[0]})


@mod.route('/edit', methods=['POST'])
def edit_author():
    form = EditAuthorForm(request.form)
    if form.validate():
        author = Author.query.filter_by(id=form.author_id.data).first()
        author.name = form.name.data
        db.session.commit()
        return json.dumps({'success': True, 'name': form.name.data})
    return json.dumps({'success': False, 'error': form.name.errors[0]})


@mod.route('/remove', methods=['POST'])
def remove_author():
    author_id = request.form.get('author_id')
    if author_id:
        author = Author.query.filter_by(id=author_id).first()
        db.session.delete(author)
        db.session.commit()
    return json.dumps({'success': bool(author_id)})
from wtforms import Form, StringField, IntegerField
from wtforms.validators import Required, ValidationError
from sqlalchemy import and_

from models import Author


class AddAuthorForm(Form):
    def author_exist(self, field):
        if Author.query.filter_by(name=self.name.data).first():
            raise ValidationError('Author already exists')

    name = StringField('Author', [Required(), author_exist])


class EditAuthorForm(Form):
    def author_exist(self, field):
        if Author.query.filter(and_(Author.name == self.name.data, Author.id != self.author_id.data)).first():
            raise ValidationError('Author already exists')

    author_id = IntegerField('Id')
    name = StringField('Author', [Required(), author_exist])
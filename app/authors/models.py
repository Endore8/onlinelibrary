from app import db


class Author(db.Model):
    __tablename__ = 'authors'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Author %r>' % self.name
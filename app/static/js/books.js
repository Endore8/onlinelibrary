$(function () {
    var csrfToken = $.cookie('_csrf_token');

    $.ajaxSetup({
        crossDomain: false,
        beforeSend: function (xhr, settings) {
            if (!(/^(GET|HEAD|OPTIONS|TRACE)$/.test(settings.type))) {
                xhr.setRequestHeader('X-CSRFToken', csrfToken);
            }
        }
    });

    $(document).on('click', '#selected-list .remove-selected', function (event) {
        event.preventDefault();

        var listItem = $(this).parent();
        listItem.slideUp('fast', function () {
            listItem.remove();
            $('#to-select').append(listItem);
            listItem.slideDown('fast', function () {
            });
        });
    });

    $(document).on('click', '#to-select .option', function (event) {
        event.preventDefault();

        var listItem = $(this).parent();
        listItem.slideUp('fast', function () {
            listItem.remove();
            $('#selected-list').append(listItem);
            listItem.slideDown('fast', function () {
            });
        });
    });

    $(document).on('click', '#add-book', function (event) {
        event.preventDefault();

        var title = $("#bookName").val();
        var authors = [];

        $('#selected-list a.option').each(function (i) {
            authors.push($(this).attr('id').split('-')[1]);
        });

        $.ajax({
            type: 'POST',
            url: 'http://localhost:5000/books/add',
            data: {
                'title': title,
                'authors': authors
            },
            success: function (data) {
                var json = $.parseJSON(data);
                if (json.success) {
                    location.reload();
                } else {
                    $('#add-book-error').text(json.error);
                }
            }
        });
    });

    $(document).on('click', '.remove-item', function (event) {
        event.preventDefault();

        if (confirm('Are you sure?')) {
            var book_item = $(this).parent().parent();
            $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/books/remove',
                data: {
                    'book_id': book_item.attr('id').split('-')[1]
                },
                success: function (data) {
                    if ($.parseJSON(data).success) {
                        book_item.remove();
                    }
                }
            });
        }
    });

    var editId = undefined;

    $(document).on('click', '.edit-item', function (event) {
        event.preventDefault();

        $('#edit-selected-list li').each(function () {
            var item = $(this);
            item.remove();
            $('#edit-to-select').append(item);
        });

        var rowId = $(this).parent().parent().attr('id');
        editId = rowId;
        var currentName = $('#' + rowId + ' td.col-1').html();
        $('#newBookName').val(currentName);

        var bookAuthors = $('#' + rowId + ' ul.related li');

        if (bookAuthors.length) {
            $('#edit-to-select li a.option').each(function () {
                var id = $(this).attr('id').split('-')[1];
                var el = $(this).parent();
                $(bookAuthors).each(function (i, item) {
                    var iId = $(item).attr('id').split('-')[1];
                    if (id == iId) {
                        el.remove();
                        $('#edit-selected-list').append(el);
                    }
                });
            });
        }

        $('#edit-book-error').html('');
        $('#editBookModal').modal('show');
    });

    $(document).on('click', '#edit-book', function (event) {
        if (editId) {
            var authors = [];
            $('#edit-selected-list a.option').each(function (i) {
                authors.push($(this).attr('id').split('-')[1]);
            });

            $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/books/edit',
                data: {
                    'book_id': editId.split('-')[1],
                    'title': $('#newBookName').val(),
                    'authors': authors
                },
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json.success) {
                        location.reload();
                    } else {
                        $('#edit-book-error').html(json.error);
                    }
                }
            });
        }
    });

    $(document).on('click', '#edit-selected-list .remove-selected', function (event) {
        event.preventDefault();

        var listItem = $(this).parent();
        listItem.slideUp('fast', function () {
            listItem.remove();
            $('#edit-to-select').append(listItem);
            listItem.slideDown('fast', function () {
            });
        });
    });

    $(document).on('click', '#edit-to-select .option', function (event) {
        event.preventDefault();

        var listItem = $(this).parent();
        listItem.slideUp('fast', function () {
            listItem.remove();
            $('#edit-selected-list').append(listItem);
            listItem.slideDown('fast', function () {
            });
        });
    });

    $(document).on('click', '.search', function (event) {
        event.preventDefault();

        var text = $('#input-search').val();

        if (text.length > 0)
            location = 'http://localhost:5000/books?q=' + text;
        else
            location = 'http://localhost:5000/books'
    });
});
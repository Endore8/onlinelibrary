$(function () {
    var csrfToken = $.cookie('_csrf_token');

    $.ajaxSetup({
        crossDomain: false,
        beforeSend: function (xhr, settings) {
            if (!(/^(GET|HEAD|OPTIONS|TRACE)$/.test(settings.type))) {
                xhr.setRequestHeader('X-CSRFToken', csrfToken);
            }
        }
    });

    $(document).on('click', '#add-author', function (event) {
        event.preventDefault();

        var name = $("#authorName").val();

        $.ajax({
            type: 'POST',
            url: 'http://localhost:5000/authors/add',
            data: {
                'name': name
            },
            success: function (data) {
                var json = $.parseJSON(data);
                if (json.success) {
                    location.reload();
                } else {
                    $('#add-author-error').text(json.error);
                }
            }
        });
    });

    $(document).on('click', '.remove-item', function (event) {
        event.preventDefault();

        if (confirm('Are you sure?')) {
            var author_item = $(this).parent().parent();
            $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/authors/remove',
                data: {
                    'author_id': author_item.attr('id').split('-')[1]
                },
                success: function (data) {
                    if ($.parseJSON(data).success) {
                        author_item.remove();
                    }
                }
            });
        }
    });

    var editId = undefined;

    $(document).on('click', '.edit-item', function (event) {
        event.preventDefault();

        var rowId = $(this).parent().parent().attr('id');
        editId = rowId;
        var currentName = $('#' + rowId + ' td.col-1').html();
        $('#newAuthorName').val(currentName);

        $('#edit-author-error').html('');
        $('#editAuthorModal').modal('show');
    });

    $(document).on('click', '#edit-author', function (event) {
        if (editId) {
            $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/authors/edit',
                data: {
                    'author_id': editId.split('-')[1],
                    'name': $('#newAuthorName').val()
                },
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json.success) {
                        $('#' + editId + ' td.col-1').html(json.name);
                        $('#editAuthorModal').modal('hide');
                        editId = undefined;
                    } else {
                        $('#edit-author-error').html(json.error);
                    }
                }
            });
        }
    });

    $(document).on('click', '.search', function (event) {
        event.preventDefault();

        var text = $('#input-search').val();

        if (text.length > 0)
            location = 'http://localhost:5000/authors?q=' + text;
        else
            location = 'http://localhost:5000/authors'
    });
});
from flask import Flask, request, session, abort, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.seasurf import SeaSurf

app = Flask(__name__)
app.config.from_pyfile('config.py')

db = SQLAlchemy(app)
csrf = SeaSurf(app)


@app.route('/')
def index():
    return render_template('index.html')


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html')


from app.users.views import mod as usersModule
from app.authors.views import mod as authorsModule
from app.books.views import mod as booksModule
app.register_blueprint(usersModule)
app.register_blueprint(authorsModule)
app.register_blueprint(booksModule)


@app.before_request
@usersModule.before_request
@authorsModule.before_request
@booksModule.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.pop('_csrf_token', None)
        if not token or (token != request.form.get('_csrf_token') and token != request.headers.get('X-CSRFToken')):
            abort(403)


def random_string():
    import string
    import random
    lst = [random.choice(string.ascii_letters + string.digits) for n in xrange(24)]
    return "".join(lst)


def generate_csrf_token():
    print 'generation new csrf token'
    if '_csrf_token' not in session:
        session['_csrf_token'] = random_string()
    return session['_csrf_token']


app.jinja_env.globals['csrf_token'] = generate_csrf_token